﻿/*Stephen Richards
 * 100458273    
 * Feb 4,2019
 * 
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class DoorProperties
{
    [SerializeField]
    bool isHot=false;
    [SerializeField]
    bool isNoisy=false;
    [SerializeField]
    bool isSafe=false;
    [SerializeField]
    bool isSet=false;

    public bool IsHot
    {
        get
        {
            return isHot;
        }

        set
        {
            isHot = value;
        }
    }

    public bool IsNoisy
    {
        get
        {
            return isNoisy;
        }
        set
        {
            isNoisy = value;
        }
    }

    public bool IsSafe
    {
        get
        {
            return isSafe;
        }
        set
        {
            isSafe = value;
        }
    }

    public bool IsSet
    {
        get
        {
            return isSet;
        }
        set
        {
            isSet = value;
        }
    }
}
