﻿/*Stephen Richards
 * 100458273    
 * Feb 4,2019
 * 
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerCameraController : MonoBehaviour
{
    public float movementSpeed = 10;
    public float turningSpeed = 60;

    public GameObject death;
    public GameObject interacting;
    public GameObject success;

    Door currentDoorInteractingWith;

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal") * turningSpeed * Time.deltaTime;
        transform.Rotate(0, horizontal, 0);

        float vertical = Input.GetAxis("Vertical") * movementSpeed;
        Vector3 velocity = transform.rotation * new Vector4(0, 0, vertical, 1.0f);
        velocity.y = GetComponent<Rigidbody>().velocity.y;
        GetComponent<Rigidbody>().velocity = velocity;
        //  transform.Translate(0, 0, vertical);

        if (Input.GetKeyDown(KeyCode.Space)) {
            Ray footRay = new Ray(transform.position, new Vector3(0, -1, 0));
            if (Physics.Raycast(footRay, 1.0f))
                GetComponent<Rigidbody>().AddForce(new Vector3(0, 200, 0));
        }

        if (currentDoorInteractingWith!=null &&Input.GetKeyDown(KeyCode.E))
        {
         

            if (currentDoorInteractingWith.myProperties.IsSafe==false)
            {
                //Time.timeScale = 0;
                interacting.gameObject.SetActive(false);
                death.gameObject.SetActive(true);
                StopAllCoroutines();
                StartCoroutine(LoadStart());
            }


            if (currentDoorInteractingWith.myProperties.IsSafe == true)
            {
                //Time.timeScale = 0;
                interacting.gameObject.SetActive(false);
                death.gameObject.SetActive(false);
                success.gameObject.SetActive(true);
                StopAllCoroutines();
        
                StartCoroutine(LoadStart());
            }
        }


    }

    IEnumerator LoadStart()
    {
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadSceneAsync(0);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Door>()!=null)
        {
            currentDoorInteractingWith = other.GetComponent<Door>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        currentDoorInteractingWith = null;
    }

}
