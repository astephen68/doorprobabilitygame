﻿/*Stephen Richards
 * 100458273    
 * Feb 4,2019
 * 
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public string[] lines;

    public Door[] myDoors;

    [SerializeField]
    public float[,,] probabilites;


    void Start()
    {
        probabilites = new float[8,1,4];
        lines = ProbabilityHandler.instance.GetFileInformation();
        if (lines!=null)
        {
            int pos = 0;
            for (int i = 1; i < lines.Length; i++)
            {
                string percent = "";
                lines[i]=Regex.Replace(lines[i], @"\s", "");
                //Debug.Log(lines[i]);
                foreach (char item in lines[i])
                {
                    
                    if (pos<3)
                    {
                        probabilites[i - 1, 0, pos] = (item=='Y' ? 1 : 0);
                        pos++;

                    }
                    else
                    {
                        percent += item;
                    }
                    
                }
                probabilites[i - 1, 0, 3] = float.Parse(percent);
                pos = 0;
                
            }
        }
        SetUpDoors();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void SetUpDoors()
    {
        int count = 0;
        for (int i = 0; i < probabilites.GetLength(0); i++)
        {
            Door door;
            DoorProperties doorToSetup = new DoorProperties();
            for (int j = 0; j < probabilites.GetLength(1); j++)
            {
                for (int ki = 0; ki < probabilites.GetLength(2); ki++)
                {
                    if (ki==0)
                    {
                        doorToSetup.IsHot = (probabilites[i, j, ki]==0?false:true);
                    }
                    if(ki==1)
                    {
                        doorToSetup.IsNoisy = (probabilites[i, j, ki] == 0 ? false : true);
                    }

                    if(ki==2)
                    {
                        doorToSetup.IsSafe = (probabilites[i, j, ki] == 0 ? false : true);
                    }
                    if (ki==3)
                    {
                        
                        float number= Mathf.Round(20.0f*probabilites[i,j,ki]);
                        for (float l = 0; l < number; l++)
                        {
                            door = myDoors[Random.Range(0, myDoors.Length)];
                            bool answer = true;
                            while (answer==true && count <20)
                            {
                                if (door.myProperties.IsSet == false)
                                {
                                    answer = false;
                                    count++;
                                }
                                else
                                {
                                    door = myDoors[Random.Range(0, myDoors.Length)];
                                }


                            }

                            door.myProperties = doorToSetup;
                            doorToSetup.IsSet = true;
                            
                        }
                    }
                }
            }
        }
    }
}
