﻿/*Stephen Richards
 * 100458273    
 * Feb 4,2019
 * 
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    public DoorProperties myProperties;
    public Canvas UiCanvas;
    private ParticleSystem ps;
    private bool isStarted;
    // Start is called before the first frame update
    void Start()
    {
        UiCanvas = GameObject.Find("UI").GetComponent<Canvas>();
        ps = gameObject.GetComponentInChildren<ParticleSystem>();
        ps.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (myProperties.IsHot)
        {
            //GetComponent<MeshRenderer>().material = Resources.Load<Material>("Material/HotDoor") as Material;
            if(!ps.isPlaying)
            {
                ps.Play();
            }

        }
        if (myProperties.IsNoisy)
        {
            if (gameObject.GetComponent<AudioSource>()==null)
            {
                AudioSource source = gameObject.AddComponent<AudioSource>();
                source.clip = Resources.Load<AudioClip>("Sounds/Scary") as AudioClip;
                source.volume = 0.5f;
            }

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.GetComponent<AudioSource>()!=null)
        {
            AudioSource source = gameObject.GetComponent<AudioSource>();
            source.Play();
        }
        UiCanvas.transform.GetChild(0).gameObject.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
        UiCanvas.transform.GetChild(0).gameObject.SetActive(false);
    }
}
