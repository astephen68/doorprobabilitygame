﻿/*Stephen Richards
 * 100458273    
 * Feb 4,2019
 * 
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public sealed class ProbabilityHandler
{
    private static readonly object padlock = new object();
    private static ProbabilityHandler Instance;


    private string[] lines;
    public static ProbabilityHandler instance {
        get {
            lock (padlock)
            {
                if (Instance == null)
                {
                    Instance = new ProbabilityHandler();

                }
                return Instance;
            }
        }     
    } 
    ProbabilityHandler()
    {

    }

    public void ReadProbabilityFile(string filename, string location)
    {
        lines = System.IO.File.ReadAllLines(location + "\\" + filename);
    }

    public string[] GetFileInformation()
    {
        if (lines!=null)
        {
            return lines;
        }

        return null;
    }


}
