﻿/*Stephen Richards
 * 100458273    
 * Feb 4,2019
 * 
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public Button StartButton;
    public InputField Location;
    public InputField FileName;
    public Text Errors;
    public int SceneToLoad;
    public void Start()
    {
        StartButton.onClick.AddListener(LoadScene);
    }

    private void LoadScene()
    {
        if (Location.text != "" && FileName.text != "")
        {
            ProbabilityHandler.instance.ReadProbabilityFile(FileName.text, Location.text);
            SceneManager.LoadSceneAsync(SceneToLoad);
        }
        else
        {
            Errors.text = "One of the fields is empty, please re enter!";
        }
    }
}
